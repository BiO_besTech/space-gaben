package levels;

import backgrounds.Background;
import backgrounds.GameBackground;
import backgrounds.GameOverBackground;
import sprites.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class Board extends JPanel implements ActionListener {

    private final int ICRAFT_X = 40;
    private final int ICRAFT_Y = 60;
    private final int DELAY = 1;
    private Timer timer;

    private static final int ENEMY_SPAWN_SPEED = 50;

    private Background gameBackground;
    private Background gameOverBackground;
    private SpaceShip spaceShip;
    private java.util.List<Enemy> enemies;
    private java.util.List<Explosion> explosions;

    public Board() {
        initBoard();
    }

    private void initBoard() {
        addKeyListener(new TAdapter());
        setBackground(Color.BLACK);
        setFocusable(true);

        gameBackground = new GameBackground();
        gameOverBackground = new GameOverBackground();
        spaceShip = new SpaceShip(ICRAFT_X, ICRAFT_Y);
        enemies = new ArrayList<>();
        explosions = new ArrayList<>();

        timer = new Timer(DELAY, this);
        timer.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        doDrawing(g);
        Toolkit.getDefaultToolkit().sync();
    }

    private void doDrawing(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        // background
        g2d.drawImage(gameBackground.getBackground(), 0, 0, this);

        // spaceship
        if (spaceShip.isVisible()) {
            g2d.drawImage(spaceShip.getImage(), spaceShip.getX(),
                    spaceShip.getY(), this);
        }

        // player missiles
        java.util.List<Missile> missiles = spaceShip.getMissiles();
        for (Missile missile : missiles) {
            g2d.drawImage(missile.getImage(), missile.getX(),
                    missile.getY(), this);
        }

        // enemy missiles
        for (Enemy enemy : enemies) {
            java.util.List<Missile> enemyMissiles = enemy.getMissiles();
            for (Missile missile : enemyMissiles) {
                g2d.drawImage(missile.getImage(), missile.getX(),
                        missile.getY(), this);
            }
        }

        // enemies
        for (Enemy enemy : enemies) {
            g2d.drawImage(enemy.getImage(), enemy.getX(),
                    enemy.getY(), this);
        }

        // explosions
        for (Explosion explosion : explosions) {
            g2d.drawImage(explosion.getImage(), explosion.getX(), explosion.getY(), this);
            explosion.increaseAge();
        }

        if (spaceShip.isPlayerDead()) {
            g2d.drawImage(gameOverBackground.getBackground(), 0, 0, this);
            timer.stop();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        updateMissiles(spaceShip);
        for (Enemy enemy : enemies) {
            updateMissiles(enemy);
        }
        updateSpaceShip();
        tickSpaceship();
        moveEnemies();
        tickEnemies();
        killEnemies();
        killExplosions();
        repaint();
    }

    private void killExplosions() {
        explosions.removeIf(explosion -> !explosion.isVisible());
    }

    private void updateMissiles(Spacecraft spacecraft) {
        java.util.List<Missile> missiles = spacecraft.getMissiles();
        for (int i = 0; i < missiles.size(); i++) {
            Missile missile = missiles.get(i);
            if (missile.isVisible()) {
                missile.move();
            } else {
                missiles.remove(i);
            }
        }
    }

    private void updateSpaceShip() {
        spaceShip.move();
    }

    private void tickSpaceship() {
        // skip if player is already dead
        if (!spaceShip.isVisible()) {
            return;
        }

        for (Enemy enemy : enemies) {
            for (Missile missile : enemy.getMissiles()) {
                if (isPlayerKilled(missile)) {
                    killPlayer(missile);
                }
            }
        }
    }

    private boolean isPlayerKilled(Missile missile) {
        if (spaceShip.getBounds().intersects(missile.getBounds())) {
            return true;
        } else {
            return false;
        }
    }

    private void spawnEnemy() {
        Enemy enemy = new Enemy(400, (int) ((Math.random() * (200) + 20)));
        enemies.add(enemy);
    }

    private void moveEnemies() {
        for (Enemy enemy : enemies) {
            enemy.move();
        }
    }

    private void tickEnemies() {
        if ((int) ((Math.random() * (ENEMY_SPAWN_SPEED))) == 1) {
            spawnEnemy();
        }

        for (Enemy enemy : enemies) {
            enemy.fire();
        }
    }

    private void killEnemies() {
        for (Missile missile : spaceShip.getMissiles()) {
            for (Enemy enemy : enemies) {
                if (isEnemyKilled(enemy, missile)) {
                    killEnemy(enemy, missile);
                    return;
                }
            }
        }
    }

    private void killEnemy(Enemy enemy, Missile missile) {
        explosions.add(new Explosion(missile.getX(), missile.getY()));
        missile.setVisible(false);
        enemies.remove(enemy);
    }

    private void killPlayer(Missile missile) {
        explosions.add(new Explosion(missile.getX(), missile.getY()));
        missile.setVisible(false);
        spaceShip.setVisible(false);
    }

    private boolean isEnemyKilled(Enemy enemy, Missile missile) {
        if (enemy.getBounds().intersects(missile.getBounds())) {
            return true;
        }

        return false;
    }

    private class TAdapter extends KeyAdapter {
        @Override
        public void keyReleased(KeyEvent e) {
            spaceShip.keyReleased(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {
            spaceShip.keyPressed(e);
        }
    }
}
