package backgrounds;

import javax.swing.*;
import java.awt.*;

public class GameOverBackground implements Background {

    public Image getBackground() {
        ImageIcon background = new ImageIcon("src/resources/game_over_background.png");
        return background.getImage();
    }
}
