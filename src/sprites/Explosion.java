package sprites;

import java.util.ArrayList;

public class Explosion extends Sprite {

    private static final int LIFETIME = 50;

    private int age;

    public Explosion(int x, int y) {
        super(x, y);
        initExplosion();
    }

    private void initExplosion() {
        loadImage("src/resources/explosion.png");
        getImageDimensions();
    }

    public void increaseAge() {
        age++;

        if(age > LIFETIME) {
            setVisible(false);
        }
    }

    public int getAge() {
        return age;
    }
}
