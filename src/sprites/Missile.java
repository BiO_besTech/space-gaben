package sprites;

public class Missile extends Sprite {

    private final int MISSILE_SPEED = 5;
    private boolean isPlayer;

    public Missile(int x, int y, boolean isPlayer) {
        super(x, y);

        this.isPlayer = isPlayer;
        initMissile();
    }

    private void initMissile() {
        if (this.isPlayer){
            loadImage("src/resources/hl2_resized.png");
        } else {
            loadImage("src/resources/ea_sports_resized.png");
        }
        getImageDimensions();
    }

    public void move() {
        if (isPlayer) {
            x += MISSILE_SPEED;
            y += (int) ((Math.random() * (5 - -5)) + -5);
        } else {
            x -= MISSILE_SPEED / 2;
            y -= (int) ((Math.random() * (5 - -5)) + -5);
        }

    }

}
