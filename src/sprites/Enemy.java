package sprites;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Enemy extends Sprite implements Spacecraft {

    private int ENEMY_SPEED = 1;
    private int ENEMY_FIRE_SPEED = 50;

    private List<Missile> missiles;

    public Enemy(int x, int y) {
        super(x, y);
        initEnemy();
    }

    public List<Missile> getMissiles() {
        return missiles;
    }

    public void fire() {
        if ((int) ((Math.random() * (ENEMY_FIRE_SPEED))) == 1) {
            missiles.add(new Missile(x + width, y + height / 2, false));
        }
    }

    private void initEnemy() {
        loadImage("src/resources/ea.png");
        getImageDimensions();
        missiles = new ArrayList<>();
    }

    public void move() {
        x -= ENEMY_SPEED;
    }

}
