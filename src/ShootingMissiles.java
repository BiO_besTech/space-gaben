import levels.Board;

import javax.swing.*;
import java.awt.*;

public class ShootingMissiles extends JFrame {

    public ShootingMissiles() {
        initUI();
    }

    private void initUI() {
        add(new Board());

        setSize(400, 300);
        setResizable(false);

        setTitle("Space-Gaben");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            ShootingMissiles ex = new ShootingMissiles();
            ex.setVisible(true);
        });
    }
}
