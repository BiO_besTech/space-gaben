package backgrounds;

import javax.swing.*;
import java.awt.*;

public class GameBackground implements Background {

    public Image getBackground() {
        ImageIcon background = new ImageIcon("src/resources/background.png");
        return background.getImage();
    }
}
