package sprites;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class SpaceShip extends Sprite implements Spacecraft {

    private int dx;
    private int dy;
    private static final int SPACESHIP_SPEED = 3;
    private List<Missile> missiles;

    private static final int DEAD_TIMER = 100;
    private int timerToGameOverScreen = 0;

    public SpaceShip(int x, int y) {
        super(x, y);
        initSpaceShip();
    }

    public boolean isPlayerDead() {
        // if player is not dead, skip timer increment
        if (isVisible()) {
            return false;
        }

        timerToGameOverScreen++;
        if (timerToGameOverScreen > DEAD_TIMER) {
            return true;
        } else {
            return false;
        }
    }

    private void initSpaceShip() {
        missiles = new ArrayList<>();
        loadImage("src/resources/gaben.png");
        getImageDimensions();
    }

    public void move() {
        x += dx;
        y += dy;
    }

    public List<Missile> getMissiles() {
        return missiles;
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_SPACE) {
            fire();
        }

        if (key == KeyEvent.VK_LEFT) {
            dx = -SPACESHIP_SPEED;
        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = SPACESHIP_SPEED;
        }

        if (key == KeyEvent.VK_UP) {
            dy = -SPACESHIP_SPEED;
        }

        if (key == KeyEvent.VK_DOWN) {
            dy = SPACESHIP_SPEED;
        }
    }

    public void fire() {
        missiles.add(new Missile(x + width, y + height / 2, true));
    }

    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT) {
            dx = 0;
        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = 0;
        }

        if (key == KeyEvent.VK_UP) {
            dy = 0;
        }

        if (key == KeyEvent.VK_DOWN) {
            dy = 0;
        }
    }

}
