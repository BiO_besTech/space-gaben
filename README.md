# README #

### What is this repository for? ###

* PoC for Space-Invaders-like Java Swing game 
* Version 0.1

### How do I get set up? ###

* Main Class: ShootingMissiles
  * Game automatically starts upon running the main class.
  * Upon dying, you need to close & re-run the main class.

### Game Rules

* You are Gaben, floating around space.
  * Press the Arrow Keys to move up, down, left and right on the Game Board.
  * Press Spacebar to shoot Lambdas (missiles) - take note of the wave patterns they have!
* EA logos are your Enemies - they will spawn from the right side of the screen, and move slowly and menacingly towards you.
  * Enemies Shoot EA Sports logos towards you - avoid those or you will DIE!
  * Shoot Enemies down with your Lambdas.
  * If you DIE, the Heavy Weapons Guy will tell you so. 

### Contribution guidelines ###

* Not open to contributions atm.

### Who do I talk to? ###

* You talk to me - Lambog.