package sprites;

import java.util.List;

public interface Spacecraft {

    public List<Missile> getMissiles();
}
