package backgrounds;

import java.awt.*;

public interface Background {

    public Image getBackground();
}
